# WordPress with CERN SSO  Quickstart

This repository implements a way of quickly deploying WordPress to OpenShift, with a CERN SSO integration.

## Deploying WordPress in OpenShift

### Request a PaaS Website instance
Go to http://cern.ch/web and request a Paas instance.
You will receive a completion mail. 
When done, go back to http://cern.ch/web, under manage my website, click *Manage your site* below *Openshift application tools* to open the Openshift console.

### Selecting the template
In the OpenShift web console home page *Get started with your project* (or from to top right menu *Add to project*), click *Import YAML/JSON*, and upload the appropriate template file, or copy/paste its content in the form.
Start from a predefined template below, changes can be done afterwards.

- Site restricted to authenticated users: https://gitlab.cern.ch/wordpress/wordpress-quickstart/raw/master/templates/cern-sso-access-restricted-to-authenticated-users.yaml
- Site restricted to an e-group: https://gitlab.cern.ch/wordpress/wordpress-quickstart/raw/master/templates/cern-sso-access-restricted-to-egroup.yaml
- Site accessible anonymously: https://gitlab.cern.ch/wordpress/wordpress-quickstart/raw/master/templates/cern-sso-no-access-restriction.yaml

Check *Process the template*, NOT *Save template*, and *Continue*.

### Entering parameters
Two parameters are mandatory, and should be carefully entered otherwise the deployment will fail:
- **APPLICATION_NAME**: must be the name of the WordPress instance, must be EXACTLY the web sitename you registered, used in http://sitename.web.cern.ch and http://cern.ch/sitename
- **ADMIN_EMAIL**: The email address of the WordPress instance administrator: your mail! REQUIRED, only this email will match the Wordpress admin account and allow admin login.

If you selected the template with E-Groups access restriction, edit the parameter:
- AUTHORIZED_GROUPS

**Leave all the other parameters untouched.**

## Changing SSO access control for visitors
Adding / removing egroups, changing access mode:
Open OpenShift Web console on this project, click *Resources* -> *Config Maps* -> *cern-sso-proxy* and edit authorize.conf for ```<Location "/">``` as you would do on your normal Apache config.

When done:
Got to *Applications* -> *Deployments* -> *cern-sso-proxy*  and click *Deploy*

> Note: this method is temporary, when the new Authorization Service will be connected, changing SSO access control will be user friendly via the Authorization Service portal.

## Adding administrators, moderators to your Wordpress instance
From the *Wordpress Dashboard menu* -> *Users* 
Add new users with specific roles, ensuring you fill in correctly the email address of the user you want to give access to.

## Changing Theme
By default the Wordpress theme is not the best, it is strongly encouraged to change it ASAP via 
From the *Wordpress Dashboard menu* -> *Appearance* -> *Themes* 

## Delete and redeploy from scratch:
With Openshift command line, select the right project (with caution !) and run:
oc delete all,configmap,pvc,serviceinstance --all
